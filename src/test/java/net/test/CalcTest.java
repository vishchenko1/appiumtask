package net.test;


import com.test.BaseTest;
import com.test.Pages;
import io.appium.java_client.TouchAction;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CalcTest extends BaseTest {


    @Test
    public void additionTest(){
        (new TouchAction(driver)).tap(100, 1415).perform();
        Pages.calculatorPage().clickNumberButton("5");
        Pages.calculatorPage().clickPlus();
        Pages.calculatorPage().clickNumberButton("8");
        Pages.calculatorPage().clickEquals();
        Assert.assertEquals(Pages.calculatorPage().checkResultField(), "7");
    }
    @Test
    public void substractionTest(){
        Pages.calculatorPage().clickNumberButton("5");
        Pages.calculatorPage().clickMinus();
        Pages.calculatorPage().clickNumberButton("8");
        Pages.calculatorPage().clickEquals();
        Assert.assertEquals(Pages.calculatorPage().checkResultField(), "3");
    }
    @Test
    public void multiplicationTest(){
        Pages.calculatorPage().clickNumberButton("2");
        Pages.calculatorPage().clickMultiply();
        Pages.calculatorPage().clickNumberButton("4");
        Pages.calculatorPage().clickEquals();
        Assert.assertEquals(Pages.calculatorPage().checkResultField(), "32");
    }
    @Test
    public void divisionTest(){
        Pages.calculatorPage().clickNumberButton("3");
        Pages.calculatorPage().clickDivide();
        Pages.calculatorPage().clickNumberButton("5");
        Pages.calculatorPage().clickEquals();
        Assert.assertEquals(Pages.calculatorPage().checkResultField(), "1.8");
    }


}
