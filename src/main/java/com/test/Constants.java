package com.test;

public class Constants {

    static final String APP_PACKAGE = "com.google.android.calculator";
    static final String APP_NAME = "Calculator";
    static final String APP_ACTIVITY = "com.android.calculator2.Calculator";
    static final String PLATFORM = "Android";
    static final String VERSION = "8.1.0";
    static final String DEVICE_NAME = "49b042fd";
}
