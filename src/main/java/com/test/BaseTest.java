package com.test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.net.URL;

import static com.test.Constants.*;

public class BaseTest {
    protected static AppiumDriver driver;
    @BeforeTest
    public void setUp() throws Exception {
        File filePath = new File(System.getProperty("user.dir"));
        File appDir = new File(filePath, "/lib/app");
        File app = new File(appDir, "Calculator.apk");
        DesiredCapabilities capabilities = DesiredCapabilities.android();
        System.out.println(app.getAbsolutePath());

        capabilities.setCapability("platformVersion", VERSION);
        capabilities.setCapability("platformName", PLATFORM);
        capabilities.setCapability("deviceName",DEVICE_NAME);

        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", Constants.APP_PACKAGE);
        capabilities.setCapability("appActivity", Constants.APP_ACTIVITY);
        capabilities.setCapability(CapabilityType.APPLICATION_NAME, Constants.APP_NAME);
        capabilities.setCapability(MobileCapabilityType.NO_RESET, false);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");


        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub")
                , capabilities);
    }
    @AfterTest
    public void tearDown() {
        driver.quit();
    }


}
