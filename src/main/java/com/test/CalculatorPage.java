package com.test;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class CalculatorPage {
    private By plusButton = By.xpath("//android.widget.Button[@content-desc=\"plus\"]");
    private By minusButton = By.xpath("//android.widget.Button[@content-desc=\"minus\"]");
    private By resultField = By.xpath("//android.widget.LinearLayout/android.widget.TextView");
    private By equalsButton = By.xpath("//android.widget.Button[@content-desc=\"equals\"]");
    private By multiplyButton = By.xpath("//android.widget.Button[@content-desc=\"multiply\"]");
    private By divideButton = By.xpath("//android.widget.Button[@content-desc=\"divide\"]");

    private MobileElement currentMobileElement;

    public void clickNumberButton(String s){
        String tempString;
        tempString = String.format("//android.widget.Button[%s]", s);
        currentMobileElement = (MobileElement)BaseTest.driver.findElement(By.xpath(tempString));
        currentMobileElement.click();
    }
    public void clickPlus(){
        currentMobileElement = (MobileElement)BaseTest.driver.findElement(plusButton);
        currentMobileElement.click();
    }
    public void clickMinus(){
        currentMobileElement = (MobileElement)BaseTest.driver.findElement(minusButton);
        currentMobileElement.click();
    }
    public String checkResultField(){
        currentMobileElement = (MobileElement)BaseTest.driver.findElement(resultField);
        return currentMobileElement.getText();
    }
    public void clickEquals(){
        currentMobileElement = (MobileElement)BaseTest.driver.findElement(equalsButton);
        currentMobileElement.click();
    }
    public void clickMultiply(){
        currentMobileElement = (MobileElement) BaseTest.driver.findElement(multiplyButton);
        currentMobileElement.click();
    }
    public void clickDivide(){
        currentMobileElement = (MobileElement) BaseTest.driver.findElement(divideButton);
        currentMobileElement.click();
    }
}
