package com.test;

public class Pages {
    private static CalculatorPage calculatorPage;

    public static CalculatorPage calculatorPage(){
        if(calculatorPage == null){
            calculatorPage = new CalculatorPage();
        }
        return calculatorPage;

    }
}
